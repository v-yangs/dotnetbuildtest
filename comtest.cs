﻿using System;
using System.Runtime.InteropServices;

[ComVisible(true),
GuidAttribute("137AD71F-4657-4362-B9E4-C6D734F1F530")]
[InterfaceType(ComInterfaceType.InterfaceIsDual)]
public interface IGetMyString
{
    string GetMyString();
}

[ComVisible(true),
GuidAttribute("89BB4535-5A89-43a0-89C5-19A4697E5C5C")]
[ProgId("CallPDW.Class1")]
[ClassInterface(ClassInterfaceType.None)]
public class Class1 : IGetMyString
{
    string IGetMyString.GetMyString()
    {
        throw new NotImplementedException();
    }
}