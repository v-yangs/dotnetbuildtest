﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace dotnetMVCrdlcTest.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult Index()
        {


            //Step 1 : Create a Local Report.
            LocalReport localReport = new LocalReport();

            //Step 2 : Specify Report Path.
            localReport.ReportPath = Server.MapPath("~/Reports/RDLC/Report1.rdlc");

            //Step 3 : Create Report DataSources
            ReportDataSource dsUnAssignedLevels = new ReportDataSource();
            dsUnAssignedLevels.Name = "UnAssignedLevels";
            //dsUnAssignedLevels.Value = dataSet.UnAssignedLevels;

            ReportDataSource dsReportInfo = new ReportDataSource();
            dsReportInfo.Name = "ReportInfo";
            //dsReportInfo.Value = dataSet.ReportInfo;

            //Step 4 : Bind DataSources into Report
            localReport.DataSources.Add(dsUnAssignedLevels);
            localReport.DataSources.Add(dsReportInfo);

            //Step 5 : Call render method on local Report to generate report contents in Bytes array
            string deviceInfo = "<DeviceInfo>" +
             "  <OutputFormat>PDF</OutputFormat>" +
             "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            string mimeType;
            byte[] renderedBytes;
            string encoding;
            string fileNameExtension;
            //Render the report          
            renderedBytes = localReport.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);


            //Step 6 : Set Response header to pass filename that will be used while saving report.
            Response.AddHeader("Content-Disposition",
             "attachment; filename=UnAssignedLevels.pdf");

            //Step 7 : Return file content result
            return new FileContentResult(renderedBytes, mimeType);
        }
    }
}